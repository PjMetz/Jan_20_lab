<!---
Heading
Ordered List
Unordered List
Horizontal Rule
Block Quote
Image
Bold
Italic
Table (this one is tricky, you might have to search for how to do it. That's part of being a developer/engineer)
Diagram (If you are feeling adventurous)

-->


# The Guitar Is The Best Instrument Ever

![Guitar Power](https://previews.123rf.com/images/theartofphoto/theartofphoto1701/theartofphoto170100184/69846910-muscular-black-man-playing-guitar-wearing-jeans-and-white-tank-top.jpg)

## My Top Three Instruments
1. Guitar
2. Saxophone
3. Violin

## Why Do I say The Guitar Is The Best?
- ***Portable***
    > A guitar can be carried anywhere, and it has a cool look! You can play without amplifications. It requires little maintenance, and all you must do is tune it.
***
- ***Affordability***
    > This make guitars a coveted instrument to start off with without breaking peoples' pockets. Beginners can find guitat for an $100, and sometimes even less.
***
- ***Flexible***
    > Guitars are suitable for most tones of music. Such as, Gospel, Hip Pop, Jazz, Rock, etc.
***
- ***Polyphonic***
    > Lastly, but not the end why the guitar is the best instrument is that it is polyphonic.   What do I mean? Guitars has a capability of playing more than one tone at a time which is why it is polyphonic. Saxophone are not polyphonic!

## Some Terminology

| Terminology      | Description                                                                      |
| ---------------- | --------------------------------------------------------------------------       |
| Neck             | The long wooden stem that connects the headstock of the guitar to the body.      |
| Fretbar          | Strips of metal found on the fretboard or fingerboard(located on the neck).      |
| Fret             | The space between each fretbar. This is where you will finger each note.         |
| Strings          | Metal or nylon strips of wire that produce sound through vibrations.             |
| Bridge           | A metal or wooden part on the front of the body that holds the strings in place. |



